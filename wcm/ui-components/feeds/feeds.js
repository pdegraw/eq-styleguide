//feeds start
var feedApiURL = "";
var hostName = window.location.href;

// if (hostName.indexOf('10080'))
//     feedApiURL = "http://" + window.location.hostname + ":10080/api/getLatestFeed/";
// else
//     feedApiURL = "http://" + window.location.hostname + "/api/getLatestFeed/";
feedApiURL = "/api/getLatestFeed/";
//console.log("feedRestURL:" + feedApiURL);


var getFeeds = function(feedType, feedCount, callback) {
    console.log("Function called FeedType:" + feedType + ",FeedCount" + feedCount);
    var items = [];
    var dataObject = {};

    function feedInput(url, size, type, actionResult, returnMessage) {
        this.url = url;
        this.size = size;
        this.type = type;
        this.actionResult = actionResult;
        this.returnMessage = returnMessage;
    };


    if (feedType == "Blog") {
        dataObject = new feedInput("https://blog.equinix.com/feed/", feedCount, "BLOG", "", "");
    } else if (feedType == "Forum") {
        dataObject = new feedInput("https://forum.equinix.com/rss/feed/all", feedCount, "FORUM", "", "");
    } else if (feedType == "PR") {
        dataObject = new feedInput("http://equinix.mediaroom.com/api/newsfeed_releases/list.php", feedCount, "NEWSFEED", "", "");
    } else {
        alert("Feed type:" + feedType + " Not found");
    }



    if (!jQuery.isEmptyObject(dataObject)) {
        $.ajax({
            url: feedApiURL,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            type: "put",
            dataType: "json",
            data: JSON.stringify(dataObject),
            success: function(response) {
                items = response.itemList;
                console.log("Response:" + JSON.stringify(response));
                callback(items);
            },
            error: function(xhr) {
                console.log("Warning:FeedAPI URL request failure");
            }
        });
    } else {
        console.log("Error: Check the dataObject");
    };
};

var populateFeed = function(domObjIn) {
    //    console.log("populate feed function call");
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var feedCount = $(domObjIn).attr("data-count");
    var feedCategory = $(domObjIn).attr("data-category");
    //    var feedBodyVar = $(domObjIn).find('.feedBody');
    var feedBodyVar = $(domObjIn);

    if (feedCategory == "Blog" || feedCategory == "Forum" || feedCategory == "PR") {
        getFeeds(feedCategory, feedCount, function(returnValue) {
            var feedsObj = JSON.parse(JSON.stringify(returnValue));
            var feedToView = "";

            //            console.log("Obje Received:"+ JSON.stringify(returnValue));
            //            console.log("ReturnLength:"+returnValue.length);
//            console.log("Headline:" + feedsObj[0]["headline"] + "-  URL:" + feedsObj[0]["url"] + "-  releaseDate:" + feedsObj[0]["releaseDate"]);


            feedBodyVar.html("<h3>This is Blog:" + feedsObj.length + "</h3>");
            for (var i = 0; i < feedsObj.length; i++) {

                if (feedCategory != "PR") {
                    var feedURL = feedsObj[i].link;
                    var feedTitle = JSON.stringify(feedsObj[i].title).substring(1, JSON.stringify(feedsObj[i].title).length - 1);
                    //                console.log("String:" + JSON.stringify(feedsObj[i]));
                    var dateToView = JSON.stringify(feedsObj[i]["publish-date"]).substring(1, JSON.stringify(feedsObj[i]["publish-date"]).length - 1);

                    dateToView = new Date(dateToView);
                    dateToView = monthNames[dateToView.getMonth()] + " " + dateToView.getDate() + ", " + dateToView.getFullYear();

                    feedToView = feedToView + "<li class=\"list-group-item\"><h4><a href=\"  " + feedURL + " \">" + feedTitle + "</a></h4><p class=\"list-group-item-text\">" + dateToView + "</p></li>";
                } else {
                    var feedURL = feedsObj[i]["url"];
                    var feedTitle = feedsObj[i]["headline"];
                    var dateToView = feedsObj[i]["releaseDate"];

                    dateToView = new Date(dateToView);
                    dateToView = monthNames[dateToView.getMonth()] + " " + dateToView.getDate() + ", " + dateToView.getFullYear();

                    feedToView = feedToView + "<li class=\"list-group-item\"><h4><a href=\"  " + feedURL + " \">" + feedTitle + "</a></h4><p class=\"list-group-item-text\">" + dateToView + "</p></li>";
                }


            };
            //            console.log("FeedToView:" + feedToView);
            feedBodyVar.html(feedToView);
        })

    } else {
        feedBodyVar.html("<h3>Please update feeds component to show:" + feedCategory + "</h3>");
    }

};

$(function() {

    $('.eq-feeds').each(function(i, ele) {
        //console.log("Index:"+i+"   ,URL:"+$(this).find('.feedUrl').html());
        populateFeed(this);
    });
});
