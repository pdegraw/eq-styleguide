$(function(){
  $('#eq-footer-fixed .dropdown').hover(
    function() {
      $(this).addClass("open");
    },
    function() {
      $(this).removeClass("open");
    }
  );
  var menu = $("#eq-footer-fixed");
  $(window).scroll(function(){
    if($(window).scrollTop() >= 100 ){
      menu.addClass('on');
    } else {
      menu.removeClass('on');
    }
  });
});
$('#getInTouchModal').on('shown.bs.modal', function () {
  $('#getInTouchModal').focus()
})
