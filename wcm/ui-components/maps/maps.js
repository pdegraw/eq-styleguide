// Code goes here
if (typeof google  !== 'undefined'){
    alert("google unknown");

console.log("Loading maps.js");

//var exicuteMaps = function(){

        console.log("Executing maps");
        var mapConfigJson = "";
        var ibxJson = "";
        var infowindow = new google.maps.InfoWindow();
        var eq_theme = new google.maps.StyledMapType(
            [
                {
                    elementType: "geometry",
                    stylers: [{
                        color: "#ededee"
                    }]
                }, {
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#523735"
                    }]
                },
                {
                    featureType: "administrative.locality",
                    elementType: "labels",
                    stylers: [{
                        "visibility": "off"
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#c9b2a6"
                    }]
                }, {
                    featureType: "administrative.land_parcel",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#dcd2be"
                    }]
                }, {
                    featureType: "administrative.land_parcel",
                    elementType: "labels.text.fill",
                    //stylers: [{color: "#ae9e90"}]
                    stylers: [{
                        "visibility": "off"
                    }]
                }, {
                    featureType: "landscape.natural",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ededee"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{
                        color: "#CBD8E3"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#93817c"
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#c0d7b4"
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#447530"
                    }]
                }, {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{
                        color: "#fff"
                    }]
                }, {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [{
                        color: "#fdfcf8"
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry",
                    stylers: [{
                        color: "#D1D3D4"
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#A5A9B9"
                    }]
                }, {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry",
                    stylers: [{
                        color: "#D1D3D4"
                    }]
                }, {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#A5A9B9"
                    }]
                }, {
                    featureType: "transit.line",
                    elementType: "geometry",
                    stylers: [{
                        color: "#CBD8E3"
                    }]
                }, {
                    featureType: "transit.line",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#8f7d77"
                    }]
                }, {
                    featureType: "transit.station",
                    elementType: "geometry",
                    stylers: [{
                        color: "#CBD8E3"
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#cbd8e3"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#CBD8E3"
                    }]
                }
            ], {
                name: "Reset Map"
            }
        );




        var loadMapWDG = function(mapContainerIn, mapTypeIn, mapInfoIn){
            console.log("****Input info from dotCMS:"+mapTypeIn+"-"+mapInfoIn);

            var latIn = 37.376901;
            var lngIn = -122.997735;
            //var mapFromHTML = $(".eq-map-body")[0];
            var mapFromHTML = mapContainerIn;
            var mapZoom = 7;
            var mapsInputJson = {};
            if(mapTypeIn === "Global")
                {
                   // console.log("*****************mapConfigJson:"+JSON.stringify(mapConfigJson["Metros"]));
                    mapsInputJson = mapConfigJson["Metros"];
                    mapZoom = mapConfigJson["Views"]["_Global"]["map-zoom"];
                }
            else if(mapTypeIn === "Region" )
                {

                    for (var countryIn in mapConfigJson["Region"][mapInfoIn]["countries"])
                        {
                            var countryInVal = mapConfigJson["Region"][mapInfoIn]["countries"][countryIn];
                            console.log("********country In:"+countryInVal);
                            for(var metroInKey in mapConfigJson["Countries"][countryInVal]["metros"])
                                {
                                    var metroInVal = mapConfigJson["Countries"][countryInVal]["metros"][metroInKey];
                                    mapsInputJson[metroInVal] = mapConfigJson["Metros"][metroInVal];
                                }
                        }
                    mapZoom = mapConfigJson["Views"]["_Region"]["map-zoom"];
                }
            else if(mapTypeIn === "Countries" )
                {

                    for (var metroIn in mapConfigJson["Countries"][mapInfoIn]["metros"])
                        {
                            var metroInVal = mapConfigJson["Countries"][mapInfoIn]["metros"][metroIn];
                            console.log("********country In:"+metroInVal);
                            mapsInputJson[metroInVal] = mapConfigJson["Metros"][metroInVal];
                        }
                    mapZoom = mapConfigJson["Views"]["_Country"]["map-zoom"];
                }
            else if(mapTypeIn === "Metros" )
                {
                    //console.log("****metros:"+JSON.stringify(ibxJson));
                    for (var ibxIn in mapConfigJson["Metros"][mapInfoIn]["ibxs"])
                        {
                            var ibxInVal = mapConfigJson["Metros"][mapInfoIn]["ibxs"][ibxIn];
                            console.log("********Ibx In:"+ibxInVal);
                            mapsInputJson[ibxInVal] = ibxJson["Ibx"][ibxInVal];
                        }

                    latIn = mapConfigJson["Metros"][mapInfoIn].latitude;
                    lngIn = mapConfigJson["Metros"][mapInfoIn].longitude;
                    mapZoom = mapConfigJson["Views"]["_Metro"]["map-zoom"];
                }
            else if(mapTypeIn === "Ibx" )
                {
                    //console.log("****metros:"+JSON.stringify(ibxJson));

                    mapsInputJson[mapInfoIn] = ibxJson["Ibx"][mapInfoIn];
                    latIn = ibxJson["Ibx"][mapInfoIn].latitude;
                    lngIn = ibxJson["Ibx"][mapInfoIn].longitude;
                    mapZoom = mapConfigJson["Views"]["_IBX"]["map-zoom"];
                }
            else
                {
                    console.log("Input this mapsInputJson");
                }

            latLng = new google.maps.LatLng(latIn, lngIn);
             var options = {
                        center: latLng,
                        zoom: parseInt(mapZoom),
                        zoomControl: true,
                        mapTypeControl: false, // default buttons
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM,
                            style: google.maps.ZoomControlStyle.LARGE
                        },
                        gestureHandling: 'cooperative',
                        mapTypeControlOptions: {
                            mapTypeIds: ["eq_theme"]
                        }
                    };


             var map = new google.maps.Map(mapFromHTML, options);
              map.mapTypes.set("eq_theme", eq_theme);
              map.setMapTypeId("eq_theme");


              // ---------------------  map reset control ---------------------
              console.log("latlng:"+latLng);

              // var chicago = {lat: 41.85, lng: -87.65};
              // chicago.lat = latIn;
              // chicago.lng = lngIn;

              var chicago = {lat: Number(latIn), lng: Number(lngIn)};
              console.log("after---------:"+JSON.stringify(chicago));
/**
 * The CenterControl adds a control to the map that recenters the map on
 * Chicago.
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
function CenterControl(controlDiv, map) {

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = '#fff';
  controlUI.style.border = '2px solid #fff';
  controlUI.style.borderRadius = '3px';
  controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
  controlUI.style.cursor = 'pointer';
  controlUI.style.marginTop = '22px';
  controlUI.style.marginLeft = '22px';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to recenter the map';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.color = 'rgb(25,25,25)';
  controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
  controlText.style.fontSize = '16px';
  controlText.style.lineHeight = '38px';
  controlText.style.paddingLeft = '5px';
  controlText.style.paddingRight = '5px';
  controlText.innerHTML = 'Center Map';
  controlUI.appendChild(controlText);

  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener('click', function() {
    map.setCenter(chicago);
  });

}

var centerControlDiv = document.createElement('div');
  var centerControl = new CenterControl(centerControlDiv, map);

  centerControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);
              // ---------------------  map reset control ---------------------



            for (var key in mapsInputJson) {
                 var lat = mapsInputJson[key].latitude;
                 var lng = mapsInputJson[key].longitude;

                 var icon = new google.maps.MarkerImage("/images/icons/markers.png", new google.maps.Size(15, 15), new google.maps.Point(1, 3));
                 if(mapsInputJson[key]["type"] === "ibx")
                         icon = new google.maps.MarkerImage("/images/icons/markers.png", new google.maps.Size(24, 20), new google.maps.Point(16, 3));
                  console.log(mapsInputJson[key]);
                //console.log("***lati and long:"+mapsInputJson[key].latitude+"-"+mapsInputJson[key].longitude);
                  //var showInfo = true;

                  var pinlatLng = new google.maps.LatLng(lat, lng);

                  marker = new google.maps.Marker({
                      position: pinlatLng,
                      icon: icon,
                      animation: google.maps.Animation.DROP,
                      map: map
                  });

                  google.maps.event.addListener(marker, 'click', function() {

                  map.panTo(marker.getPosition());
                   if(mapsInputJson[key].type == "ibx")
                   {
                     infowindow.setContent('<div class="text-center"><h4>'+mapsInputJson[key]["popup-linktext"]+'</h4><p>'+mapsInputJson[key]["data"]+'</p> </div>');
                   }

                   else
                   infowindow.setContent('<div class="text-center"><a href="'+mapsInputJson[key]["url"]+'"><h4>'+mapsInputJson[key]["popup-linktext"]+'</h4></a></div>');

                   infowindow.open(map, this);
                   //map.setCenter(marker.getPosition());
                 });
                 console.log("Map type:"+mapsInputJson[key].type);
                 if(mapsInputJson[key].type =="ibx")
                 {
                   google.maps.event.trigger( marker, 'click' );
                 }
                 google.maps.event.addDomListener(window, 'resize', function() {
                   infowindow.open(map, marker);
                 });
            }
            console.log("map-load-end");
        };




        jQuery(".eq-map-body").each(function(i, obj) {
            //alert("inside maps container:"+jQuery(this)[0]);
            var mapTypeIn = jQuery(this).find(".eq-map-data").attr("data-map-type");
            var mapInfoIn = jQuery(this).find(".eq-map-data").attr("data-map-info");
            var mapDomIn = jQuery(this)[0];






            //--------------Logic to download JSON object as needed

            var getMapConfig = function(callback){
                $.getJSON("/application/data/mapConfig.json", function(result){
                    mapConfigJson = result;
                    // console.log("*******mapConfig has been loaded - Type Of:"+ typeof funcIn);
                    if(callback)
                        callback();
                });
            };

            var exicuteLoadMapWDG = function(){
                loadMapWDG(mapDomIn,mapTypeIn,mapInfoIn);
            };

            var getIbxs = function(){

                $.getJSON("/application/data/ibx.json", function(result){
                    ibxJson = result;
                    console.log("******ibxs json has been loaded:"+ typeof funcIn);
//                    if(funcIn === "loadMap")
                        loadMapWDG(mapDomIn,mapTypeIn,mapInfoIn);
//                    else
//                        {
//                            funcIn;
//                        }

                });
            }

            if(mapTypeIn !== "Custom")
                {
                     if(mapTypeIn === "Metros" || mapTypeIn === "Ibx"){
                         console.log("****map type in metro or Ibx:"+mapTypeIn);
                         if(mapConfigJson ===""){
                            //  console.log("11111111111");
                             getMapConfig(getIbxs);
                         }
                         else if(ibxJson === ""){
                            //  console.log("22222222222");
                             getIbxs();
                         }
                         else{
                            //  console.log("3333333333333");
                             loadMapWDG(mapDomIn,mapTypeIn,mapInfoIn);
                         }
                     }
                    else{
                        // console.log("****map type in Global,Region,Country");
                        if(mapConfigJson ==="")
                            getMapConfig(exicuteLoadMapWDG);
                        else
                            loadMapWDG(mapDomIn,mapTypeIn,mapInfoIn);
                    }
                }
            else
                {
                    console.log("This is a custom map component");
                }

        });

$('.eq-map-overlay-close').on('click', function(){
    $( this ).parent().remove();
});
$('.eq-maps-loading').delay(1000).fadeOut(700);
}
else {
  console.log("********** Missing google maps script CDN ***************");
}
