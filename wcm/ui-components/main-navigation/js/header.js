
// Start Header Functions ------------------------------------------------------------------------------------------>

$(document).ready(function() {

    // Variables to check the window type to execute desktop or mobile functions
    var windowType;
    var $wind = $(window);
    // Set variables
    var subNavOpen = false, mousedOverSubNav, mainNavItem = $('#eq-main-navbar .eq-dropdown'), currentNavItem, subMenu = $('.EQ-main-nav-dropdown'), mainNav = $('#eq-main-navbar ul.nav');
    // Function to open menu
    function openMenu(){
      subMenu.slideDown(500);
      subNavOpen = true;
    }
    // Function to close menu
    function closeMenu(){
      if(!mousedOverSubNav && !mousedOverNavItem){
      subMenu.slideUp(500);
      subNavOpen = false;
      }
    }
    // All functions executed on desktop screen
    var desktopFunctions = function() {

            // If mobile submenu is open, close it for desktop view
            if($(".EQ-mobile-main-nav-dropdown").hasClass("in")){
                $(".EQ-mobile-main-nav-dropdown").removeClass("in");
            }

            // Sticky Header Fade In Begin -------------------------->
            var header = $('#eq-main-header'), scrollPosition, logoFull = $("#eq-logo-full"), logoSmall = $("#eq-logo-sm"), logoText =$("#eq-logo-text");
            $(window).scroll(function(){
                if ($(window).scrollTop() > 107){
                  header.addClass('minimal');
                  logoFull.toggleClass('logo-hide', true);
                  logoText.toggleClass('logo-hide', true);
                  logoSmall.toggleClass('logo-hide', false);
                  $('.equinix-level-one-template > .container').css('margin-top', '107px');
                } else {
                  header.removeClass('minimal');
                  logoFull.toggleClass('logo-hide', false);
                  logoText.toggleClass('logo-hide', false);
                  logoSmall.toggleClass('logo-hide', true);
                  $('.equinix-level-one-template > .container').css('margin-top', '0');
                }
                if ($(window).scrollTop() > 115){
                  header.addClass('menu-fade');
                } else {
                  header.removeClass('menu-fade');
                }
           });
           // Sticky Header Fade In End -------------------------->

            // Remove click action toggling mobile submenu
            $('.eq-dropdown').attr('data-toggle', '');

            // Sliding Menu Animation Begin -------------------------->

            // Function to check if utility nav is open - main nav sub menu should not open if utility nav is open
            function isUtilityNavOpen(){
              var isOpen = $('#eq-utility-navbar li').hasClass('open');
              return isOpen;
              }

            // As mouse enters main nav, find related drop down
            mainNavItem.on('mouseenter', function(e){
              currentNavItem = $(this), navItem = $(this).find('a.dropdown-toggle').data("dropdown"), subMenuId = navItem + "-sub-menu";
              var edropdown = $('.EQ-main-nav-dropdown #' + subMenuId );
              mousedOverNavItem = true;
              $('.EQ-main-nav-dropdown .container').toggleClass('active', false);
              $('.EQ-main-nav-dropdown #' + subMenuId ).toggleClass('active', true);
            });

            // Assign mouseenter event on UL, then delegate events to the current Nav Item
            mainNav.on('mouseenter', currentNavItem, function(e){
                var timeOutObj;
                function menuOpenDelay(toggledMenu) {
                      timeOutObj = setTimeout(function(){
                          if(!subNavOpen && !isUtilityNavOpen()){
                            setTimeout(openMenu, 10);
                          }
                          mousedOverNavItem = true;
                      }, 200);
                  }
                // Stop slidedown from executing immediately when nav is hovered over
                function stopMenuTimeout() {
                      clearTimeout(timeOutObj);
                }

               menuOpenDelay($(this));
               $(this).mouseleave(stopMenuTimeout);

            });
            // Set action for hovering over drop down sub menu
            subMenu.on('mouseenter', function(e){
              mousedOverSubNav = true;
              subNavOpen = true;
            });
            // Set events for cursor leaving main nav
            mainNav.on('mouseleave', function(e){
              mousedOverNavItem = false;
                  setTimeout(closeMenu, 200);
            });
            // Set events for cursor leaving submenu
            subMenu.on('mouseleave', function(e){
              mousedOverSubNav = false;
                    setTimeout(closeMenu, 200);
            });
            // Sliding Menu Animation End -------------------------->
        };

        // All functions executed on mobile screen
        var mobileFunctions = function() {

            // Reset if mobile accordion submenus are open
            var mainNavItem = $('#eq-main-navbar .eq-dropdown');
            for(var i = 0; i < mainNavItem.length; i++){
                var currentNavItem = mainNavItem[i];
                var isMainItemOpen = $(currentNavItem).hasClass('collapsed');
                if(!isMainItemOpen){
                    $(currentNavItem).toggleClass('collapsed', true);
                    var currentIcon = $(currentNavItem).find('i')[0];
                    $(currentIcon).toggleClass('fa-chevron-right fa-chevron-down');
                }
            }

            // Add click action on mobile
            $('.eq-dropdown').attr('data-toggle', 'collapse');

            // toggleText function
            jQuery.fn.extend({
               toggleText: function (a, b){
                 var isClicked = false;
                 var that = this;
                 this.click(function (){
                   if (isClicked) { that.find('.toggle-text').text(a); isClicked = false; }
                   else { that.find('.toggle-text').text(b); isClicked = true; }
                 });
                 return this;
               }
            });

            // Mobile Menu Text Toggle
            $('.navbar-toggle').toggleText('Menu', 'Close');

        };

    // Check window size and execute desktop or mobile functions accordingly
    var mobileCheck = function() {
            var window_w=$wind.width();
            var currType = window_w < 768 ? 'mobile' :'desktop';
             if( windowType==currType){
                 return;
             }else{
                 windowType=currType;
             }

            if(windowType=='mobile') {
                mobileFunctions();
            } else {
                desktopFunctions();
            }
        };

    mobileCheck();
    var resizeTimer;

    // Check window width on resize with 300ms delay before running mobilecheck();
    $(window).resize(function() {
        if(resizeTimer) {
            clearTimeout(resizeTimer);
        }
        resizeTimer = setTimeout(mobileCheck, 50);
    });

    // Icon toggle on mobile
    function checkClassPresentForMobile(currentObject,currentIconName,newIconName){
        var icon = $(currentObject).find("i")[0];
        if($(icon).hasClass(currentIconName)){
            $(icon).removeClass(currentIconName);
            $(icon).addClass(newIconName);
        }
    }


    // Main Nav Link Click ACtion
    $("#mainNavAccordion li").on("click", function(e) {
        // Get window width
        var windowWidth = $(window).width();

        if (windowWidth < 768) {
            e.preventDefault();
            // Open mobile subnav
            if ($(this).hasClass("collapsed") === true) {
                checkClassPresentForMobile($(this),"fa-chevron-right","fa-chevron-down");
            }

            // Close mobile subnav
            else {
                checkClassPresentForMobile($(this),"fa-chevron-down","fa-chevron-right");
            }
        }
    });

    // Set events for cursor entering and leaving submenu outside of mobile views.
    // Set action for hovering over drop down sub menu
    var subMenu = $('.EQ-main-nav-dropdown'), mainNav = $('#eq-main-navbar ul.nav');
    subMenu.on('mouseenter', function(e){
      mousedOverSubNav = true;
      subNavOpen = true;
    });
    // Set events for cursor leaving main nav
    mainNav.on('mouseleave', function(e){
      mousedOverNavItem = false;
      setTimeout(closeMenu, 200);
    });
    // Set events for cursor leaving submenu
    subMenu.on('mouseleave', function(e){
      mousedOverSubNav = false;
        setTimeout(closeMenu, 200);
    });

    // Mobile Menu Icon Toggle
    $('.navbar-toggle').click(function(){
        $(this).find('span.fa').toggleClass('fa-bars fa-close');
    });

});

// End Header Functions ------------------------------------------------------------------------------------------>
