$(function(){
    "use strict";
    var holder = $(".eq-mcf-ctrl"),
        contentPlus = $(".eq-mcf .eq-content-plus"),
        btnPlus = $(".eq-mcf .eq-btn-plus");

    holder.on("mouseover", function() {
        $(this).children(".eq-content").css("display", "block");
        $(".btn .fa-inverse", this).addClass("eq-active");
    });

    holder.on("mouseout", function() {
        $(this).children(".eq-content").css("display", "none");
        $(".btn .fa-inverse", this).removeClass("eq-active");
    });

    btnPlus.on("click", function() {
        $(this).siblings().children(".fa-inverse").addClass("eq-active");
        if (contentPlus.css("left") === "-153px") {
            contentPlus.animate({
                "left": 0,
            }, 500);
            btnPlus.animate({
                "left": "153px",
            }, 500);
        } else {
            contentPlus.animate({
                "left": "-153px",
            }, 500);
            btnPlus.animate({
                "left": 0,
            }, 500);
            $(this).siblings().children(".fa-inverse").removeClass("eq-active");
        }
    });
});
    
