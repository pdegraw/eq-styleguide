// /* global $ */
//
// $(function(){
//     'use strict';
//     $('.mktoForm').ready(function() {
//         var form = $('.mktoForm');
//         $(form).each(function() {
//             // set the Marketo form width
//             $(this).css({
//                 'width': '100%',
//                 'max-width': '600px'
//             });
//             $(this).find('mktoHtmlText').css({
//                 'width': '100%',
//                 'max-width': '600px'
//             });
//             // add some style to the Marketo rows with two columns
//             var row = $('.mktoFormRow');
//             $(row).each(function() {
//                 var column = $(this).find('.mktoFormCol');
//                 if ( column.length > 1 ) {
//                     $(column.first()).css({
//                         'width': '48%',
//                         'margin-right': '2%'
//                     });
//                     $(column.last()).css({
//                         'width': '50%',
//                         'margin-right': '0%'
//                     });
//                 }
//             });
//         });
//     });
// });
