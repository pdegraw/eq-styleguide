// Start Secondary Navigation

$(function() {
    // console.log('start secondary-nav.js');
    var button = $('#secondaryNavButton'),
        nav = $('#collapseSecondaryNavMenu'),
        iconOpen = 'fa-caret-down',
        iconClose = 'fa-caret-up',
        resizeTimer = '';

    // button toggle ui
    $(button).click(function() {
        $(this).find('i.fa').toggleClass(iconOpen + ' ' + iconClose);
    });

    // reset button to default state
    function resetButton() {
        console.log(button);
        $(button).find('i').removeClass(iconClose).addClass(iconOpen);
        $(nav).removeClass('in');
    };

    // reset nav to default state
    function resetNav() {
        if ($(window).width() < 768) {
            resetButton();
        } else {
            $(nav).addClass('in').css({
                'height': 'auto'
            });
        }
    };

    // collapse menu on page load for mobile views and again when page is resized
    resetNav();

    // on resize, reset nav/button to default state
    $(window).on('resize', function(e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            resetNav();
        }, 250);
    });
    // console.log('end secondary-nav.js');
});

// End Secondary Navigation
