/* global $ */
// find all div.eq-captions-wrapper and get their img width, set the wrapper to that width
function captionWidth(){
    $('.eq-captions-wrapper')
    .not($(".eq-carousel-mini .eq-captions-wrapper, .eq-carousel .eq-captions-wrapper, pre .eq-captions-wrapper"))
    .each(function() {
        $(this).width('auto');
        $(this).find('img').width('auto');
        var imgWidth = $(this).find('img').width();
        $(this).width(imgWidth);

        $(this).css('min-height','auto');
        $(this).find('.caption').outerHeight('auto');
        var captionHeight = $(this).find('.caption').outerHeight();
        $(this).css('min-height',captionHeight);
        // console.log('running...');
    });
};
// Run when page loads
$(function(){
    captionWidth();
});
// Run again when page is resized
var resizeTimer;
$(window).on('resize', function(e) {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function() {
    captionWidth();
  }, 250);
});
// Add img-responsive to any image in a wysiwyg
$(function(){
    $('.wysiwyg img').each(function() {
        $(this).addClass('img-responsive');
    });
});
