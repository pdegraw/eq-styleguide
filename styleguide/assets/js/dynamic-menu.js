/*global $, jQuery, alert, SyntaxHighlighter*/

$(function () {
    'use strict';
    // Fade in Speed and Paths
    var fadeInDuration = 300,
        sgPath = '../../../styleguide',
        uiPath = '../../../wcm/ui-components',
        elements = [],
        rootList = [],
        mainMenuItems = [],
        subMenuStyleguide = [],
        subMenuUIComponents = [],
        subMenuPageTemplates = [],
        subMenuTheme = [];
    $('#eq-main-header').hide().fadeIn(fadeInDuration);

    // Set root URL path for any HTML page that does not have the correct path
    $('.navbar-brand').attr('href', '/styleguide/');


    // Menu Markup Creation Function
    function createMenu(menuToBuild) {
        $.each(menuToBuild, function () {
            var parent = elements[this.url.substr(0, this.url.lastIndexOf("/"))],
                list = parent ? parent.next("ul") : rootList,
                item = [];
            if (!list.length) {
                list = $("<ul>").insertAfter(parent);
            }
            item = $("<li>").appendTo(list);
            $("<a>").attr("href", this.url).text(this.name).appendTo(item);
            $(item).children().attr("target", this.target);
            $(item).children().attr("data-content", this.content);
            elements[this.url] = item;
        });
    }

    // Main Menu ---------------------------------------------------------------
    mainMenuItems = [
        { name: "Home", url: sgPath + "/" },
        { name: "HTML", url: sgPath + "/html" },
        { name: "Typography", url: sgPath + "/typography" },
        { name: "UI Components", url: sgPath + "/ui-components" },
        { name: "Page Templates", url: sgPath + "/page-templates" },
        { name: "Theme", url: sgPath + "/theme" }
        // { name: "Tone of Voice", url: sgPath + "/tone/index.html" }
    ];
    rootList = $("<ul class='nav navbar-nav navbar-right'>").hide().fadeIn(fadeInDuration).appendTo("#eq-main-navbar");
    elements = {};
    createMenu(mainMenuItems);

    // Submenu: Styleguide -----------------------------------------------------------
    subMenuStyleguide = [
        { name: "Content Authors", url: sgPath + "/getting-started/content-authors.html" },
        { name: "Developers", url: sgPath + "/getting-started/developers.html" },
        { name: "Release Notes", url: sgPath + "/release-notes/" }
    ];
    rootList = $("<ul class='style-guide-list'>").hide().fadeIn(fadeInDuration).appendTo("#eq-submenu-styleguide");
    elements = {};
    createMenu(subMenuStyleguide);


    // Submenu: UI Components --------------------------------------------------
    subMenuUIComponents = [
        { name: "Banners", url: uiPath + "/banners/banners.html" },
        { name: "Breadcrumb", url: uiPath + "/breadcrumb/breadcrumb.html", target: "_blank" },
        { name: "Callout", url: uiPath + "/callout/callout.html" },
        { name: "Carousel", url: uiPath + "/carousel/carousel.html", target: "_blank" },
        { name: "Carousel - Mini", url: uiPath + "/carousel-mini/carousel-mini.html" },
        { name: "Events", url: uiPath + "/events/events.html" },
        { name: "Featured Events", url: uiPath + "/featured-events/featured-events.html", target: "_blank" },
        { name: "Featured Menu Link", url: uiPath + "/fml/fml.html" },
        { name: "Footer", url: uiPath + "/footer/footer.html", target: "_blank" },
        { name: "Footer Fixed", url: uiPath + "/footer-fixed/footer-fixed.html", target: "_blank" },
        { name: "Forms", url: uiPath + "/forms/forms.html" },
        { name: "Image", url: uiPath + "/image/image.html" },
        { name: "Image Markup", url: uiPath + "/image-markup/image-markup.html" },
        { name: "Media Module", url: uiPath + "/media-module/media-module.html" },
        { name: "Modals", url: uiPath + "/modals/modals.html" },
        { name: "Multi Conversion Flag", url: uiPath + "/mcf/mcf.html", target: "_blank" },
        { name: "Related Content Module 1", url: uiPath + "/rcm/rcm.html", target: "_blank" },
        { name: "Related Content Module 2", url: uiPath + "/rcm/rcm2.html", target: "_blank" },
        { name: "Secondary Navigation", url: uiPath + "/secondary-nav/secondary-nav.html" },
        { name: "Sidebars", url: uiPath + "/sidebars/sidebars.html" },
        { name: "Tables", url: uiPath + "/tables/tables.html" },
        { name: "Tabs", url: uiPath + "/tabs/tabs.html" }
    ];
    rootList = $("<ul class='style-guide-list'>").hide().fadeIn(fadeInDuration).appendTo("#eq-submenu-ui-components");
    elements = {};
    createMenu(subMenuUIComponents);

    // Submenu: Page Templates -------------------------------------------------
    subMenuPageTemplates = [
        { name: "Full Width", url: sgPath + "/page-templates/full-width.html", target: "_blank" },
        { name: "Resources", url: sgPath + "/page-templates/resources.html", target: "_blank"  },
        { name: "Resources Category", url: sgPath + "/page-templates/resources-category.html", target: "_blank"  },
        { name: "Resources Landing - Gated", url: sgPath + "/page-templates/resources-landing-gated.html", target: "_blank"  }
    ];
    rootList = $("<ul class='style-guide-list'>").hide().fadeIn(fadeInDuration).appendTo("#eq-submenu-page-templates");
    elements = {};
    createMenu(subMenuPageTemplates);

    // Submenu: Theme Elements -------------------------------------------------
    subMenuTheme = [
        { name: "Complete Theme", url: sgPath + "/theme/theme.html", target: "_blank" },
        { name: "Main Nav Menu", url: uiPath + "/main-navigation/main-navigation.html", target: "_blank" },
        { name: "Breadcrumb", url: uiPath + "/breadcrumb/breadcrumb.html", target: "_blank" },
        { name: "Footer", url: uiPath + "/footer/footer.html", target: "_blank" },
        { name: "Footer Fixed", url: uiPath + "/footer-fixed/footer-fixed.html", target: "_blank" },
        { name: "Related Content Module", url: uiPath + "/rcm/rcm.html", target: "_blank" },
        { name: "Multi Conversion Flag", url: uiPath + "/mcf/mcf.html", target: "_blank" }
    ];
    rootList = $("<ul class='style-guide-list'>").hide().fadeIn(fadeInDuration).appendTo("#eq-submenu-theme");
    elements = {};
    createMenu(subMenuTheme);


    // Use "content: 'snippet'" in the link lists to just display the content inside the main.container
    $("a[data-content='snippet']").bind("click", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $('main.container').load(url);
    });

});

// In Page Navigation, woohoo! -------------------------------------------------
$(function () {
    "use strict";
    var pageHeaders = $(".style-guide-section").find(".page-header").not('pre .page-header'),
        links = [],
        iconLinks = [],
        scrollToTop = [];
    $(pageHeaders).each(function () {
        // Also strip out characters that interfere with the scrolling
        var linkUrl = $(this).text().replace(/ /g, '').replace(/&/g, "").replace(/-/g, "").replace(/\//g, "");
        // Add an ID of the page-header text
        $(this).attr('id', linkUrl);
        // Add each to the style guide page navigation
        $("#styleGuideLinks").append("<li><a href='#" + linkUrl + "'>" + $(this).text() + "</a></li>");
        // Add link and icon after page header
        $(this).before('<a name=' + linkUrl + '></a>');
        $(this).append('<a class="anchor" href="#' + linkUrl + '"><span class="fa fa-link"></span></a>');
    });
    // scroll to position using jQuery animate for smooth scrolling and fix position of scroll due to fixed header
    function scrollTo(e) {
        $(e).each(function () {
            $(this).on("click", function () {
                // console.log($(this).attr('href'));
                $('body,html').stop().animate({
                    scrollTop: $($(this).attr('href')).offset().top + -180
                }, 450);
            });
        });
    }
    links = $('#styleGuideLinks').find('a');
    // In the event the user clicks the Link icon to the left of the header, run the scrollTo function
    iconLinks = $('.page-header').find('a');
    scrollTo(links);
    scrollTo(iconLinks);
    // Add Scroll to Top after .style-guide-section and add jQuery animate for smooth scrolling
    scrollToTop = '<a class="scrollToTop" href="#">Scroll to Top <i class="fa fa-angle-up"></i></a>';
    $(scrollToTop).insertAfter('div.style-guide-section');
    $('.scrollToTop').on("click", function () {
        $('body,html').stop().animate({
            scrollTop: 0
        }, 300);
        return false;
    });
});

// syntaxhighlighter Defaults --------------------------------------------------
$(function () {
    "use strict";
    if (window.SyntaxHighlighter) {
        SyntaxHighlighter.defaults.toolbar = false;
        SyntaxHighlighter.defaults['auto-links'] = false;
        SyntaxHighlighter.all();
    }
});
