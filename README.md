# Web Style Guide 2016

### Developers

https://equinix.bitbucket.io/styleguide/documentation/developer/developer.html


#### Production Ready Files

*JavaScript*

	https://equinix.bitbucket.io/dist/js/scripts.js
	https://equinix.bitbucket.io/dist/js/scripts.min.js

*Cascading Style Sheets*

    https://equinix.bitbucket.io/dist/css/style.css
    https://equinix.bitbucket.io/dist/css/style.min.css
