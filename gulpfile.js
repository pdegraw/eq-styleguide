"use strict";
// include required package
var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var concat = require("gulp-concat");
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var pump = require("pump"); // To help properly handle error conditions with Node streams, recommended by gulp-uglify
var browserSync = require("browser-sync").create();

// Sets style paths
var sassFiles = ["./wcm/**/*.scss", "./styleguide/*.scss"];
var cssDest = "./dist/css";
    // Sets destination css paths
var cssFiles = "./dist/css/style.css";
var styleguideCssFiles = "./dist/css/styleguide.css";
var minDest = "./dist/css";
    // Sets JavaScript paths
var jsFiles = ["wcm/ui-components/**/*.js", "wcm/ui-components/main-navigation/**/*.js"];
var jsDest = "./dist/js";
    //Set Html paths
var htmlFiles = ["./styleguide/**/*.html", "./wcm/**/*.html"];


// SASS --------------------------------------------------->
// Sets gulp-sass options
var sassOptions = {
    errLogToConsole: true,
    outputStyle: "expanded"
};
// Sets gulp-autoprefixer options
var autoprefixerOptions = {
    browsers: ["last 2 versions", "> 5%", "Firefox ESR"]
};


// Converts SASS to CSS with gulp-sass
gulp.task("sass", function () {
    return gulp
        .src(sassFiles)
        .pipe(sass(sassOptions).on("error", sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(cssDest));
});


// Converts CSS to minified CSS with gulp-clean-css
gulp.task("minify-css", ["sass"], function () {
    return gulp.src(cssFiles)
        .pipe(cleanCSS({compatibility: "ie8"}))
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest(minDest))
        .pipe(browserSync.stream());
});

gulp.task("minify-css-styleguide", ["sass"], function () {
    return gulp.src(styleguideCssFiles)
        .pipe(cleanCSS({compatibility: "ie8"}))
        .pipe(rename("styleguide.min.css"))
        .pipe(gulp.dest(minDest));
});

// Fonts -------------------------------------------------->
// Compiles fonts into dist folder
gulp.task("fonts", function () {
    gulp.src("./wcm/assets/fonts/**/*.{ttf,woff,eof,svg,eot}")
        .pipe(gulp.dest("./dist/fonts"));
});

// Images -------------------------------------------------->
// Compiles fonts into dist folder
gulp.task("images", function () {
    gulp.src("./wcm/assets/images/*.{png,jpg,gif}")
        .pipe(gulp.dest("./dist/images"));
});

// JavaScript --------------------------------------------->
// Concatenates, Uglifies, and Minifies JS files with gulp-uglify
gulp.task("scripts", function () {
    pump([
        gulp.src(jsFiles),
        concat("scripts.js"),
        gulp.dest(jsDest),
        rename("scripts.min.js"),
        uglify(),
        gulp.dest(jsDest)
    ]);
});

// BrowserSync -------------------------------------------->
// Sets up Browser Sync
gulp.task("browserSync", function () {
    browserSync.init({
        startPath: "./styleguide",
        server: {
            baseDir: "./"
        }
    });
});

// Watch -------------------------------------------------->
// Watch for file changes in and run Browser Sync...
gulp.task("watch", ["browserSync", "sass", "minify-css", "minify-css-styleguide", "scripts"], function () {
    gulp.watch("./wcm/assets/scss/**/*.scss", ["sass"], browserSync.reload);
    gulp.watch(sassFiles, ["sass"], browserSync.reload);
    gulp.watch(styleguideCssFiles, ["minify-css-styleguide"], browserSync.reload);
    gulp.watch(cssFiles, ["minify-css"], browserSync.reload);
    gulp.watch(jsFiles, ["scripts"], browserSync.reload);
    gulp.watch(htmlFiles, browserSync.reload);
});

gulp.task("default", ["browserSync", "sass", "scripts", "fonts", "images", "watch"]);
